# Compiler
CC=g++

# Compiler options
CFLAGS=-c -Wall -fopenmp -std=c++11 -O2 $(shell sdl2-config --cflags)

# Linker options
LINKS=-lGL -lGLEW $(shell sdl2-config --libs) -lboost_program_options

# All of the .hpp header files to use as dependencies
HEADERS=ant.hpp constants.hpp draw.hpp objs.hpp path.hpp randomness.hpp rectangle.hpp food.hpp

# All of the object files to produce as intermediary work
OBJECTS=main.o ant.o draw.o objs.o path.o randomness.o rectangle.o food.o

# The final program to build
EXECUTABLE=ant_project.exe

# --------------------------------------------

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(DEBUG) $(OBJECTS) -fopenmp -o $(EXECUTABLE) $(LINKS)

%.o: %.cpp $(HEADERS)
	$(CC) $(CFLAGS) -o $@ $<

clean:
	rm $(EXECUTABLE) $(OBJECTS)
