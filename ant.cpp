#include <stdlib.h>
#include <iostream>
#include <cmath>
#include <SDL.h>

#include "ant.hpp"
#include "draw.hpp"
#include "objs.hpp"

//#include <boost/multi_array.hpp>
//#include <boost/algorithm/clamp.hpp>

//boost::multi_array<int, 2> trail(boost::extents[WINDOW_H][WINDOW_W]);

// NOTE might be useful
// Numerical directions are:
//    7 8 9
//    4 5 6
//    1 2 3
/*
int oppositeDirection(const int D)
{
	if(D >= 1 && D <= 9)
		return 10 - D;
	else
		return 0;
}
*/



Ant::Ant(){
	mode = WANDER;

	// set the initial position
	// SDL draws top-left corner
	// so we need to figure out the corrct x y coordinate so
	// ant's middle is SPAWN_LOC_X_COOR, SPAWN_LOC_Y_COOR
	position.x = SPAWN_LOC_X_COOR - ANT_WIDTH/2;
	position.y = SPAWN_LOC_Y_COOR - ANT_HEIGHT/2;

	// Initial velocity

	// There's a bunch of different variations here; none of them are perfect.
	// The only reason I even put these here is because velocity behavior
	// seems to be slightly different on our systems.

	velocity.x = random_sign();
	velocity.y = random_sign();

	//velocity.x = random_sign() * random_f(MIN_VELOCITY, MAX_VELOCITY);
	//velocity.y = random_sign() * random_f(MIN_VELOCITY, MAX_VELOCITY);

	//velocity.x = random_sign() * random_f(-ONE_MOVEMENT_UNIT, ONE_MOVEMENT_UNIT);
	//velocity.y = random_sign() * random_f(-ONE_MOVEMENT_UNIT, ONE_MOVEMENT_UNIT);

	//velocity.x = random_f(-ONE_MOVEMENT_UNIT, ONE_MOVEMENT_UNIT);
	//velocity.y = random_f(-ONE_MOVEMENT_UNIT, ONE_MOVEMENT_UNIT);

	//velocity.x = 1.0f;
    //velocity.y = 1.0f;

    //velocity.x = 0.0f;
    //velocity.y = 1.0f;

	// randomize the velocity
	changeDirection();

	// temp
	//i = 0;
	path_i = 0;
	path_dir = 0;

	trail_id = -1;

	//std::cout << velocity.x << " " << velocity.y << std::endl;
}


bool Ant::hasDeliverFood(){
    return has_deliver_food;
}



void Ant::changeDirection(){
	const static glm::vec3 v3 = glm::vec3(0.0f, 0.0f, 1.0f);
	const static glm::mat4 m4 = glm::mat4(1.0f);

	const float rad = random_angle_rad(STD_DEV_IN_RAD);
    const glm::mat4 rotate_mat = glm::rotate(m4, rad, v3);
    const glm::vec4 result = rotate_mat * glm::vec4(velocity.x, velocity.y, 0.0f, 0.0f);
    velocity.x = result.x;
    velocity.y = result.y;

	// get a new random maximum time until next movement change
	direction_time_max = random_f(ANT_MIN_TIME_MOVE, ANT_MAX_TIME_MOVE);

	// timer is reset
	direction_time_elapsed	= 0.0f;
}

void Ant::update(const float dt, Objs& objs)
{
	switch(mode)
	{
		case FOLLOW_TRAIL:
			follow_trail(objs);
			break;
		case FIND_HOME:
			find_home(dt, objs);
			break;
		case WANDER:
			wander(dt, objs);
	}
}

void Ant::takeFood()
{
	has_food = true;

	// probably will have more
}

void Ant::giveFood()
{
	has_food = false;

	// probably will have more
}

void Ant::move_randomly(const float dt, Objs& objs)
{
	// periodically change direction
	if(((direction_time_elapsed += dt) >= direction_time_max)){
        changeDirection();
	}

	const glm::vec2 newPosition = position + velocity;

	// Prevent leaving the screen
	if(	(newPosition.x < 0) || (newPosition.y < 0) ||
		(newPosition.x >= WINDOW_W - ANT_WIDTH) ||
		(newPosition.y >= WINDOW_H - ANT_HEIGHT)
	)
	{
		changeDirection();
		return;
	}

	int num_walls = objs.walls.size();
    bool did_collided = false;
#pragma omp parallel for
    for(int i = 0; i < num_walls; ++i){
        if(objs.walls[i].collision(newPosition.x, newPosition.y, ANT_WIDTH, ANT_HEIGHT)){
            did_collided = true;
        }
    }

    if(did_collided){
        changeDirection();
        return;
    }

    /* // serial collision detection
	// Prevent moving through walls
	for(auto wall : objs.walls)
	{
		if(wall.collision(newPosition.x, newPosition.y, ANT_WIDTH, ANT_HEIGHT))
		{
			changeDirection();
			return;
		}
	}
	*/


	// until we have time to improved a better path system don't
    // record path
    /*
	if(mode == WANDER)
	{
		Rectangle R(newPosition.x, newPosition.y, ANT_WIDTH, ANT_HEIGHT);

		for(unsigned int i = 0; i < known_paths.size(); i++)
		{
			Path& path = known_paths.at(i);

			int col_i = path.collision(R);

			if(col_i)
			{
				path_i = col_i;
				path_dir = 1;

				trail_id = path.id;
				mode = FOLLOW_TRAIL;

				return;
			}
		}

	}
	*/

	// check to see if it had taken any food
	// if not check for food collision
	// ants will only encounter this once
    // Prevent moving through food
    if(mode == WANDER){
        for(auto food : objs.foods)
        {
            if(food.collision(newPosition.x, newPosition.y, ANT_WIDTH, ANT_HEIGHT))
            {
                // associate ant with the food it took
                food_id = food.getID();
                takeFood();

                // change direction at next upate
                direction_time_elapsed = direction_time_max;
                //changeDirection();

                // update position to current position
                // position = newPosition;
                // return;

                // works because ant enter this function either in wondering or find home
                break; // and update position
            }
        }
    }

    if(mode == FIND_HOME){
        // Drop food and go back to get more
        for(auto home : objs.homes)
        {
            if(home.collision(newPosition.x, newPosition.y, ANT_WIDTH, ANT_HEIGHT))
            {
                giveFood();
                has_deliver_food = true;
                //changeDirection();
                //return;
                break;
            }
        }
    }

	position = newPosition;
}

void Ant::wander(const float dt, Objs& objs)
{
	move_randomly(dt, objs);

	if(has_food)
	{
        // ant encounter food
        // check if food has a path to home
        if(objs.foods[food_id].getPathSize()){
            // if it does randomly pick a value 0 or 1
            // if 1 then get the path from food
            // if 0 then randomly go home
            if(random_0_or_1()){
                path = objs.foods[food_id].getPath();
                has_path_to_home = true;
            }
            else{
                path.push_back(position);
            }
        }
        else{
            // if it doesn't keep moving
            // record first path
            path.push_back(position);
        }

        mode = FIND_HOME;

		/*
		if(known_path.points.empty())
		{
			mode = FIND_HOME;
		}
		*/

        // until we have a better path system
        // dont record path
        /*
		if(trail_id == -1)
		{
			mode = FIND_HOME;

			// trail_id is the i value of the newly created path.
			trail_id = known_paths.size();

			Path new_path;
			new_path.id = trail_id;
			known_paths.push_back(new_path);

            // Add first point
			glm::vec2 current_position(position.x, position.y);
			known_paths.at(trail_id).points.push_back(current_position);
		}
		*/
	}
}

void Ant::find_home(const float dt, Objs& objs)
{
	move_randomly(dt, objs);

    // add current position to path
	path.push_back(position);

    // if home is found ant has given the food away
    if(!has_food){
        // check the ant's path size from the path size food currently has, only if food has a path
        int food_path_size = objs.foods[food_id].getPathSize();
        if(food_path_size){
            int my_path_size = path.size();
            if(my_path_size < food_path_size){
                // update food path if the ant's path is smaller
                omp_set_lock(&objs.writelock);
                objs.foods[food_id].setPath(path);
                omp_unset_lock(&objs.writelock);
            }
            else{
                //the path that food has is smaller than the ant's path
                path = objs.foods[food_id].getPath();
            }
        }
        else{
            // this ant is the first to go home from the food source
            omp_set_lock(&objs.writelock);
            objs.foods[food_id].setPath(path);
            omp_unset_lock(&objs.writelock);
        }

        // make the ant go back and forth between food and home
        // set the index to go back to the food source
        // minus two because after this function return , ant is render at this current position
        // then the follow trail method is called so we want the ant to start moving right away.
        // instead of having to be in the home state for two frames.
        path_i = path.size() - 2;

        mode = FOLLOW_TRAIL;
    }

	// periodically add current position to path

    // until we imporved the path algorithm
    // don't check for path
    /*
	if(!has_food)
	{
		mode = FOLLOW_TRAIL;

        // comment start here
		known_path.complete = true;
		path_i = known_path.points.size() - 1;
		path_dir = -1;
        // end of comment here

		// Add last point
		//Point current_position(position.x, position.y);
		glm::vec2 current_position(position.x, position.y);
		known_paths.at(trail_id).points.push_back(current_position);

		known_paths.at(trail_id).complete = true;
		path_i = known_paths.at(trail_id).points.size() - 1;
		path_dir = -1;
	}
	else
	{
		// comment start here
		if((known_path.resolution_time_elapsed += dt) >= PATH_TIME_RESOLUTION)
		{
			//Point current_position(position.x, position.y);
			glm::vec2 current_position(position.x, position.y);
			known_path.points.push_back(current_position);

			known_path.resolution_time_elapsed = 0.0f;
		}
		// end of comment here

		if((known_paths.at(trail_id).resolution_time_elapsed += dt) >= PATH_TIME_RESOLUTION)
		{
			//Point current_position(position.x, position.y);
			glm::vec2 current_position(position.x, position.y);
			known_paths.at(trail_id).points.push_back(current_position);

			known_paths.at(trail_id).resolution_time_elapsed = 0.0f;
		}
	}
	*/

}

void Ant::follow_trail(Objs& objs)
{

    // check if ant has reached the food source
    // you want to check for negative value
    // because the ant position is updated first then it is render.
    // or
    // check if the ant has reached home
    if(path_i < 0 || path_i >= (int)path.size()){
        // check if ant still has the shortest path
        // if not get the shortest path from food
        int my_path_size = path.size();
        if(my_path_size > objs.foods[food_id].getPathSize()){
            path = objs.foods[food_id].getPath();
        }

        if(has_food){
            // if the ant is carrying food then
            // it is moving from the food source to home
            // it has reached home
            path_i = path.size() - 2;
            position = path[path_i];
            giveFood();
        }
        else{
            // if the ant is not carrying any food then
            // the ant is going from home to the food source
            // it has reached the food source
            path_i = 1;
            position = path[path_i];
            takeFood();
        }
    }
    else{
        // keep moving
        if(has_food){
            position = path[path_i++];
        }
        else{
            position = path[path_i--];
        }
    }


    // until we imporved the path finding algorithm
    // comment out this seconds of code
    /*
	Path& path = known_paths.at(trail_id);

	if(path.points.size() <= 1)
		return;
	else if(path_i == 0)
		path_dir = 1;
	else if(path_i == (path.points.size() - 1))
		path_dir = -1;

	path_i += path_dir;

	// First get point-teleportation working,
	// then worry about actually moving across the line.
	// Actually though, teleportation looks like real movement, mostly.
	position = path.points.at(path_i);
	*/
}

void Ant::draw()
{
	// 0xf1d700
	// 241,215,0
	static const float R = 241.0f/255.0f;
	static const float G = 215.0f/255.0f;
	static const float B = 0.0f;

	if(mode == FOLLOW_TRAIL)
		drawBox(position.x, position.y, ANT_WIDTH, ANT_HEIGHT, 1.0, 0.0, 0.0);
	else
		drawBox(position.x, position.y, ANT_WIDTH, ANT_HEIGHT, R, G, B);
}

void Ant::draw(std::vector<float>& all_vertices,
			   std::vector<GLuint>& all_elements,
			   const int i
			  )
{
	if(mode == FOLLOW_TRAIL)
		drawBox(position.x, position.y, ANT_WIDTH, ANT_HEIGHT, 1.0, 0.0, 0.0, all_vertices, all_elements, i);
	else
		drawBox(position.x, position.y, ANT_WIDTH, ANT_HEIGHT, ANT_COLOR_R, ANT_COLOR_G, ANT_COLOR_B, all_vertices, all_elements, i);
}
