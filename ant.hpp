/*
 * This class represent a ant.
 * The ant randomly move within the 2D environment avoiding obstacles
 * while looking for the food sources.
 *
 * It has basic memory in which it can remember its movement and the home
 * coorindate.
 *
 * The ant behavior is simplistics in that it follows only a small sets
 * of rules.
 *
 * Becuase of the limitation of time, most fields and methods are public.
 * No getter or setter
 */


#ifndef __ANT_H__
#define __ANT_H__

#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "constants.hpp"
#include "draw.hpp"
#include "path.hpp"
//#include "rectangle.hpp"
#include "randomness.hpp"

//#include <boost/multi_array.hpp>

// If an ant detects food while wandering, its path is pushed into this vector.
// Any wandering ant that collides with a known path will begin following it.
// If an ant is following a path that leads nowhere, or the path dissipates
// while the ant is following it, the ant should start wandering again.
//
// When an ant begins following a known path, its own path home should be
// emptied to save memory. In fact, maybe it's better to not even start making
// a path until the ant is holding food. Once it has food, it should wander
// until it finds home, while recording the path back to the food.

//std::vector<Path> known_paths;

// NOTE might be useful
// Return the direction that is opposite from D.
// Returns 0 for invalid directions.
// Numerical directions are:
//    7 8 9
//    4 5 6
//    1 2 3
//int oppositeDirection(const int D);

//extern boost::multi_array<Path, 2> trail_food;
//extern boost::multi_array<Path, 2> trail_home;

// Including the header would create a circular dependency,
// but this is all we need from it.
class Objs;

class Ant{
		// WANDER:			Ant is looking for food, or paths to food
		// FIND_HOME:		Ant found food, but doesn't yet know the way home
		// FOLLOW_TRAIL:	Ant knows a path from food to home, and follows it
		enum Mode {WANDER, FIND_HOME, FOLLOW_TRAIL};
		Mode mode;

		glm::vec2 velocity;

		// Need to be reorganized when they are working
		float direction_time_max;
		float direction_time_elapsed;
		//float direction_timer;

		// temp
		//unsigned long i;
		int path_i;
		int path_dir;
		int trail_id;

		bool has_food = false;
        int food_id;                        // the id of the food the ant encounter
        bool has_path_to_home = false;      // whether or not ant received a path to home from food
        std::vector<glm::vec2> path;

        // statistic to count the number of ants that had bring food home
        bool has_deliver_food = false;
                
		void move_randomly(float dt, Objs& objs);
		void wander(float dt, Objs& objs);
		void find_home(float dt, Objs& objs);
		void follow_trail(Objs& objs);

		void takeFood();
		void giveFood();

	public:
		Ant(); // set the ant color and initial location
		~Ant() = default;

		glm::vec2 position;

		// temp
		//int mode;

		void changeDirection();

		void update(const float dt, Objs& objs);

		void draw();

		void draw(std::vector<float>& all_vertices,
				  std::vector<GLuint>& all_elements,
				  const int i
				 );
        
        // for stats
        // once the ant delivered it's first food to home
        // a flag is set
        bool hasDeliverFood();
};

#endif //__ANT_H__
