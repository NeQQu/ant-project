#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#define DEFAULT_NUM_THREADS 2
#define WINDOW_W 1024.0f
#define WINDOW_H  768.0f
#define VSYNC 1
#define TITLE "Ant Simulation"

#define ANT_DRAW_COUNT 100000

// the x coordinate of the ant's spawn location, in pixels
#define SPAWN_LOC_X_COOR 500
// the y coordinate of the ant's spawn location, in pixels
#define SPAWN_LOC_Y_COOR 750

// The zone that is around the home location that the wall can not be in
#define MAP_SAFE_ZONE_DIMENSION 100.0f

// Used to periodically add a new path point, based on some condition.
// Not yet sure which we'll prefer, so I just put them both for now.
#define PATH_STEP_RESOLUTION 1.0f
#define PATH_TIME_RESOLUTION 16.67f

// Total seconds before a path gets set to expire.
// We'll need to play around with it to find something optimal.
// Should not affect paths that are in-progress.
#define PATH_DECAY_TIME 30.0f

// Colors
// We are using high contrast colors so that it's somewhat easier to see.


#define BG_COLOR_R 0.0f
#define BG_COLOR_G 0.0f
#define BG_COLOR_B (184.0f/255.0f)


#define ANT_COLOR_R (241.0f/255.0f)
#define ANT_COLOR_G (215.0f/255.0f)
#define ANT_COLOR_B 0.0f


#define WALL_COLOR_R (190.0f/255.0f)
#define WALL_COLOR_G (23.0f/255.0f)
#define WALL_COLOR_B (192.0f/255.0f)


#define FOOD_COLOR_R (1.0f/255.0f)
#define FOOD_COLOR_G (219.0f/255.0f)
#define FOOD_COLOR_B (68.0f/255.0f)

/*****************************************************************************
 * Ant's properties
 *
 * To increase the ants' movement increase MAX_VELOCITY a value of 1.0f
 * is very fast
 *
 * Change ANT_MAX_TIME_MOVE to a higher value to increase the probability
 * that the ant will travel in one direction longer
 *
 */
#define ONE_MOVEMENT_UNIT 1.0f
#define MAX_VELOCITY 1.0f		// Not used currently
#define MIN_VELOCITY 0.25f		// Not used currently
#define ANT_WIDTH 2.0f
#define ANT_HEIGHT 2.0f
#define ANT_MIN_TIME_MOVE 5.0f
#define ANT_MAX_TIME_MOVE 60.0f

#define DEFAULT_NUM_ANTS 20000

// use 30.0 or greater for largeer
// extreme
#define STD_DEV_IN_DEGREE 30.0f

// We were converting this in code, but why?
// Better to just pre-define it.
#define STD_DEV_IN_RAD 0.5235987f

/*****************************************************************************
 * wall's properties
 */
#define DEFAULT_NUM_WALLS 32
#define WALL_MIN_THICKNESS 3
#define WALL_MAX_THICKNESS 3

/*
#define WALL_MIN_HORZ_LENGTH 102.0f  // about 10% of 1024
#define WALL_MAX_HORZ_LENGTH 921.0f  // about 90% of 1024
#define WALL_MIN_VERT_LENGTH 76.0f   // about 10% of  768
#define WALL_MAX_VERT_LENGTH 691.0f  // about 90% of  768
*/

#define WALL_MIN_HORZ_LENGTH 50.0f  // about  5% of 1024
#define WALL_MAX_HORZ_LENGTH 512.0f // about 50% of 1024
#define WALL_MIN_VERT_LENGTH 38.0f  // about  7% of  768
#define WALL_MAX_VERT_LENGTH 384.0f // about 50% of  768

/*****************************************************************************
 * food's properties
 */
#define DEFAULT_NUM_FOODS 3

// The percentage of the screen that food can occupy, from top down.
// Thus, a value of 0.25f means only the top 25% of the screen can have food.
#define MAX_FOOD_DISTANCE 1.0f

#endif
