#include "draw.hpp"

void drawBox(const float X, const float Y,
				const float W, const float H,
				const float R, const float G, const float B
				)
{
	const std::vector<float> vertices {
		X,     Y,     R, G, B, 1.0f, // Top left
		X + W, Y,     R, G, B, 1.0f, // Top right
		X + W, Y + H, R, G, B, 1.0f, // Bottom right
		X,     Y + H, R, G, B, 1.0f  // Bottom left
	};

	const std::vector<GLuint> elements {
		0, 1, 2,
		2, 3, 0
	};

	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * vertices.size(), &vertices.front(), GL_DYNAMIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * elements.size(), &elements.front(), GL_DYNAMIC_DRAW);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void drawBox(const float X, const float Y,
				const float W, const float H,
				const float R, const float G, const float B,
				std::vector<float>& all_vertices,
				std::vector<GLuint>& all_elements,
				const unsigned int i
				)
{
	// i is the index number of the object that is being processed.

	const std::vector<float> vertices {
		X,     Y,     R, G, B, 1.0f, // Top left
		X + W, Y,     R, G, B, 1.0f, // Top right
		X + W, Y + H, R, G, B, 1.0f, // Bottom right
		X,     Y + H, R, G, B, 1.0f  // Bottom left
	};

	// This might look weird, but it's just making sure that the first element
	// coordinate of the next object is the last coordinate of the
	// previous object +1.
	const std::vector<GLuint> elements {
		4*i,     4*i + 1, 4*i + 2,
		4*i + 2, 4*i + 3, 4*i
	};

	// Rather than drawing now, vertices and elements get buffered for later
	// processing. This means everything can get drawn by GPU at once, greatly
	// improving performance.

	// j is the relative index of the local vector data defined above.
	// k is the global index of that data in the vectors that store everything.

	for(unsigned int j = 0; j < vertices.size(); j++)
	{
		const unsigned int k = i * vertices.size() + j;
		all_vertices[k] = vertices[j];
	}

	for(unsigned int j = 0; j < elements.size(); j++)
	{
		const unsigned int k = i * elements.size() + j;
		all_elements[k] = elements[j];
	}
}

void drawBuffered(const std::vector<float>& all_vertices,
				  const std::vector<GLuint>& all_elements
				 )
{
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * all_vertices.size(), &all_vertices.front(), GL_DYNAMIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * all_elements.size(), &all_elements.front(), GL_DYNAMIC_DRAW);

	glDrawElements(GL_TRIANGLES, all_elements.size(), GL_UNSIGNED_INT, 0);
}

