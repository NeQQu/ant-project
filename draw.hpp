#ifndef DRAW_HPP
#define DRAW_HPP

#include <vector>

// Always do this before including SDL_opengl.h
#ifndef NO_SDL_GLEXT
#define NO_SDL_GLEXT
#endif
#include <GL/glew.h>

#include <SDL_opengl.h>

void drawBox(const float X, const float Y,
				const float W, const float H,
				const float R, const float G, const float B
				);

// Buffered version of the above
void drawBox(const float X, const float Y,
				const float W, const float H,
				const float R, const float G, const float B,
				std::vector<float>& all_vertices,
				std::vector<GLuint>& all_elements,
				const unsigned int i
				);

// Draw entire buffer at once
void drawBuffered(const std::vector<float>& all_vertices,
				  const std::vector<GLuint>& all_elements
				 );

#endif
