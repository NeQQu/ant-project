#include "food.hpp"


Food::Food(const int ID, const float X, const float Y, const float W, const float H)
    : Rectangle(X, Y, W, H){
        this->ID = ID;
    }
    
int Food::getID(){ return ID; }

void Food::setID(int ID){ this->ID = ID; }

int Food::getPathSize(){
    return path.size();
}

std::vector<glm::vec2> Food::getPath(){
    std::vector<glm::vec2> temp_path = this->path;
    return temp_path;
}

void Food::setPath(std::vector<glm::vec2> path){
    this->path.clear();
    this->path = path;
}