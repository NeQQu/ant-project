#ifndef __FOOD_HPP_
#define __FOOD_HPP_

#include <glm/glm.hpp>
#include "rectangle.hpp"

class Food : virtual public Rectangle{
    int ID;
    std::vector<glm::vec2> path;
    
    public:
        Food(const int ID = 0, const float X = 0, const float Y = 0, const float W = 40, const float H = 40);
        
        int getID();
        void setID(int ID);   
        
        int getPathSize();
        std::vector<glm::vec2> getPath();
        void setPath(std::vector<glm::vec2> path);
};


#endif