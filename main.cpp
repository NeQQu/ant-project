/**
 * OpenMP Lock: https://stackoverflow.com/questions/2396430/how-to-use-lock-in-openmp
 *
 */


#include <iostream>

#include <cstdlib>
#include <vector>
#include <iterator>
#include <fstream>
#include <string>
#include <omp.h>
#include <time.h>
#include <stdlib.h>

// Always do this before including SDL_opengl.h
#ifndef NO_SDL_GLEXT
#define NO_SDL_GLEXT
#endif
#include <GL/glew.h>

#include <SDL.h>
#include <SDL_opengl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <boost/program_options.hpp>

#include "ant.hpp"
#include "constants.hpp"
#include "draw.hpp"
#include "objs.hpp"
#include "randomness.hpp"
#include "rectangle.hpp"
#include "food.hpp"


// comment #define EXPERIMENT to run the program in normal mode
// uncomment #define EXPERIMENT to run the program in experimental mode
//#define EXPERIMENT


namespace po = boost::program_options;

SDL_Window*   sdlwindow;
SDL_GLContext glcontext;

bool fullscreen  = false;
bool serial_draw = false;
bool verbose     = false;

int thread_count = DEFAULT_NUM_THREADS;

// Source:
// https://stackoverflow.com/questions/116038/what-is-the-best-way-to-read-an-entire-file-into-a-stdstring-in-c
std::string fileToString(const std::string& FILE_NAME)
{
   std::ifstream ifs(FILE_NAME);
   std::string str(std::istreambuf_iterator<char>{ifs}, {});
   return str;
}

// Source:
// https://gamedev.stackexchange.com/questions/110825/how-to-calculate-delta-time-with-sdl/123957
double getDeltaTime(){
   static Uint64 NOW = SDL_GetPerformanceCounter();
   static Uint64 LAST = 0;

   LAST = NOW;
   NOW = SDL_GetPerformanceCounter();

   return (float)((NOW - LAST)*1000 / (float)SDL_GetPerformanceFrequency() );
}

// NOTE: Don't remove this yet. We might need it for threaded tasks.
// The version that has static variables can only work for a single task.
// I tried to make a version that could be used for multiple different tasks.
// However, this one requires managing the LAST variable outside the function.
/*
double getDeltaTime2(const double& LAST){
   return (float)((SDL_GetPerformanceCounter() - LAST)*1000 / (float)SDL_GetPerformanceFrequency() );
}
*/

// dynamically generate the obstacles walls
void generateWalls(std::vector<Rectangle>& walls){
    static Rectangle safe_zone(
        SPAWN_LOC_X_COOR - (MAP_SAFE_ZONE_DIMENSION/2.0f),
        SPAWN_LOC_Y_COOR - (MAP_SAFE_ZONE_DIMENSION/2.0f),
        MAP_SAFE_ZONE_DIMENSION,
        MAP_SAFE_ZONE_DIMENSION
    );

   const int NUM_WALLS = walls.size();

    for(int i = 0; i < NUM_WALLS; ++i){
        walls[i].r=WALL_COLOR_R;
        walls[i].g=WALL_COLOR_G;
        walls[i].b=WALL_COLOR_B;
        if(i<(NUM_WALLS/2.0)){
            // verticle wall
            walls[i].w=random_f(WALL_MIN_THICKNESS,WALL_MAX_THICKNESS);
            walls[i].h=random_f(WALL_MIN_VERT_LENGTH,WALL_MAX_VERT_LENGTH);
        }
        else{
            // horizontal wall
            walls[i].w=random_f(WALL_MIN_HORZ_LENGTH,WALL_MAX_HORZ_LENGTH);
            walls[i].h=random_f(WALL_MIN_THICKNESS,WALL_MAX_THICKNESS);
        }

        // set wall location that doesn't collided into the safe zone
        do{
            walls[i].x=random_f(0.0f, WINDOW_W - walls[i].w);
            walls[i].y=random_f(0.0f, WINDOW_H - walls[i].h);
        }while(walls[i].collision(safe_zone));
    }
}

void generateFoods(std::vector<Food>& foods, const std::vector<Rectangle>& walls)
{
   const int NUM_FOODS = foods.size();

   // set food location and color dynamically
   for(int i = 0; i < NUM_FOODS; ++i){
      foods[i].r = FOOD_COLOR_R;
      foods[i].g = FOOD_COLOR_G;
      foods[i].b = FOOD_COLOR_B;

      // for quicker calulation when the ant reach home and need to get the
      // latest version of the path from Food, the food ID should match
      // the its element index inside vec
      foods[i].setID(i);

      // when running the experiment generate one food source and set it above home
#ifdef EXPERIMENT
      foods[i].x = SPAWN_LOC_X_COOR;
      foods[i].y = SPAWN_LOC_Y_COOR - 140.0f;
#else
      bool isCollision=false;

      while(true)
      {
         foods[i].x = random_f(0.0f, WINDOW_W - foods[i].w);
         foods[i].y = random_f(0.0f, MAX_FOOD_DISTANCE * (WINDOW_H - foods[i].h));

         for(auto wall : walls)
         {
            if(wall.collision(foods[i]))
            {
               isCollision=true;
               break;
            }
         }

         if(isCollision)
         {
            isCollision=false;
            // loop back, to get a different location
         }

         else
         {
            // we are good, no collision
            break;
         }
      }
#endif
   }
}

// Prepare SDL, OpenGL, etc. as necessary.
bool setup()
{
   if(SDL_Init(SDL_INIT_EVERYTHING) == -1)
      return false;

   SDL_DisableScreenSaver();

   SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

   SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                  SDL_GL_CONTEXT_PROFILE_CORE
                  );
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
   SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
   SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

   SDL_GL_SetSwapInterval(VSYNC);

   Uint32 flags = SDL_WINDOW_OPENGL;

   if(fullscreen)
      flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;

   SDL_DisplayMode dm;
   SDL_GetDesktopDisplayMode(0, &dm);

   const float window_w = (fullscreen) ? dm.w : WINDOW_W;
   const float window_h = (fullscreen) ? dm.h : WINDOW_H;

   sdlwindow = SDL_CreateWindow(TITLE,
                        SDL_WINDOWPOS_CENTERED_DISPLAY(0),
                        SDL_WINDOWPOS_CENTERED_DISPLAY(0),
                        window_w, window_h, flags
                        );

   glcontext = SDL_GL_CreateContext(sdlwindow);

   // bg color (default: 0x0000b8)
   glClearColor(BG_COLOR_R, BG_COLOR_G, BG_COLOR_B, 1.0f);

   glViewport(0, 0, window_w, window_h);

   // Enable transparency
   //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
   //glEnable(GL_BLEND);

   // Use modern OpenGL
   glewExperimental = GL_TRUE;
   glewInit();

   // Create buffer objects
   GLuint vao, vbo, ebo;
   glGenVertexArrays(1, &vao);
   glBindVertexArray(vao);
   glGenBuffers(1, &vbo);
   glBindBuffer(GL_ARRAY_BUFFER, vbo);
   glGenBuffers(1, &ebo);
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);

   // Draw to default framebuffer
   glBindFramebuffer(GL_FRAMEBUFFER, 0);

   // Modify engine coordinate system to work like SDL.
   glm::mat4 projection = glm::ortho(0.0f, window_w, window_h,
                             0.0f, -1.0f, 1.0f
                            );

   // Create shader program
   GLuint program = glCreateProgram();

   // Compile vertex shader
   GLuint vert_shad = glCreateShader(GL_VERTEX_SHADER);
   const std::string vstr = fileToString("shape.vert");
   const char* vert_src = vstr.c_str();
   glShaderSource(vert_shad, 1, &vert_src, NULL);
   glCompileShader(vert_shad);

   // Compile fragment shader
   GLuint frag_shad = glCreateShader(GL_FRAGMENT_SHADER);
   const std::string fstr = fileToString("shape.frag");
   const char* frag_src = fstr.c_str();
   glShaderSource(frag_shad, 1, &frag_src, NULL);
   glCompileShader(frag_shad);

   // Link shaders
   glAttachShader(program, vert_shad);
   glAttachShader(program, frag_shad);
   glBindFragDataLocation(program, 0, "out_color");
   glLinkProgram(program);

   // Compiled shaders can be removed after linking
   glDetachShader(program, vert_shad);
   glDetachShader(program, frag_shad);
   glDeleteShader(vert_shad);
   glDeleteShader(frag_shad);

   // Use the GLSL shader program that was just linked
   glUseProgram(program);

   // Send object positions to GLSL
   GLint position = glGetAttribLocation(program, "position");
   glEnableVertexAttribArray(position);
   glVertexAttribPointer(position, 2, GL_FLOAT, GL_FALSE,
                    6 * sizeof(float), 0
                   );

   // Send object colors to GLSL
   GLint in_color = glGetAttribLocation(program, "in_color");
   glEnableVertexAttribArray(in_color);
   glVertexAttribPointer(in_color, 4, GL_FLOAT, GL_FALSE,
                    6 * sizeof(float), (void*) (2 * sizeof(float))
                   );

   // Convert pixels to OpenGL coordinates
   // (This means you use pixel coordinates in the engine, and OpenGL
   //  will understand it as values between 0.0 and 1.0)
   GLint proj = glGetUniformLocation(program, "proj");
   glUniformMatrix4fv(proj, 1, GL_FALSE, glm::value_ptr(projection));

   // Allow zooming in (S < 1.0) or out (S > 1.0)
   // These values are just values that look sort of ok on my screen.
   GLint scale = glGetUniformLocation(program, "scale");
   glUniform1f(scale, (fullscreen) ? 0.38f : 1.0f);

   // Allow applying an X or Y offset to the screen output
   // These values are just values that look sort of ok on my screen.
   GLint offs = glGetUniformLocation(program, "offs");
   glUniform2f(offs, (fullscreen) ? 200.0f : 0.0f, (fullscreen) ? 20.0f : 0.0f);

   return true;
}

void drawSerial(Objs& objs)
{
   // Render homes below everything
   for(auto home : objs.homes)
      home.draw();

   // Render walls below everything but homes
   for(auto wall : objs.walls)
      wall.draw();

   // Render the food locations
   for(auto food : objs.foods)
      food.draw();

   // Draw each ant directly to screen, one-by-one
   for(auto ant : objs.ants)
      ant.draw();
}

void drawParallel(Objs& objs,
                  std::vector<float>& all_vertices,
                  std::vector<GLuint>& all_elements
                 )
{
   unsigned long buff_cnt = 0;

   // NOTE: We could use parallel for here, but we will need to come
   //       up with a way to do it that doesn't need a critical section
   //       for buff_cnt, otherwise it won't help much.

   for(auto home : objs.homes)
      home.draw(all_vertices, all_elements, buff_cnt++);

   for(auto wall : objs.walls)
      wall.draw(all_vertices, all_elements, buff_cnt++);

   for(auto food : objs.foods)
      food.draw(all_vertices, all_elements, buff_cnt++);


//#pragma omp parallel for num_threads(thread_count)
   for(unsigned int i = 0; i < objs.ants.size(); ++i){
   //for(unsigned int i = 0; i < ANT_DRAW_COUNT; ++i){
      objs.ants[i].draw(all_vertices, all_elements, buff_cnt++);
    }

    //std::cout << all_vertices.size() << ", " << all_elements.size() << std::endl;

   // Parallel GPU draw of all buffered objects
   drawBuffered(all_vertices, all_elements);
}

int main(int argc, char* argv[])
{
   // Parse command line arguments
   // ------------------------------

   int ants  = DEFAULT_NUM_ANTS;
   int walls = DEFAULT_NUM_WALLS;
   int foods = DEFAULT_NUM_FOODS;

   po::options_description desc("Options");
   desc.add_options()
      ("ants,a",    po::value<int>(&ants),         "Set number of ants in simulation")
      ("help,h",                                   "Print this help message and exit")
      ("foods,f",   po::value<int>(&foods),        "Set number of food areas in simulation")
      ("fullscreen,F",                             "Run the program in fullscreen")
      ("sdraw,s",                                  "Use the serial drawing algorithm")
      ("threads,t", po::value<int>(&thread_count), "Set number of threads for simulation logic")
      ("verbose,v",                                "Print runtime info to console")
      ("walls,w",   po::value<int>(&walls),        "Set number of walls in simulation")
   ;

   po::positional_options_description pop;
   pop.add("ants", 1);
   pop.add("walls", 1);
   pop.add("foods", 1);

   po::variables_map vm;
   po::store(po::command_line_parser(argc, argv).options(desc).positional(pop).run(), vm);
   po::notify(vm);

   if (vm.count("help"))
   {
      std::cout << std::endl;
      std::cout << desc << std::endl;
      std::cout << "Usage:" << std::endl;
      std::cout << "  ./ant_project.exe [options] [<num ants>] [<num walls>] [<num foods>]" << std::endl;
      std::cout << std::endl;
      return 0;
   }
   else
   {
      fullscreen  = vm.count("fullscreen");
      serial_draw = vm.count("sdraw");
      verbose     = vm.count("verbose");
   }

   if(verbose)
   {
      printf("Threads:\t%d\n", thread_count);
      printf("Ant  count:\t%d\n", ants);
      printf("Wall count:\t%d\n", walls);
      printf("Food count:\t%d\n", foods);
      printf("Draw mode:\t%s\n", (serial_draw) ? "Serial" : "Parallel");
   }

   // Set up libraries
   // ------------------------------

   if(!setup())
      return -1;

   // The SDL_Event object keeps track of various system events, like
   // keyboard & mouse input, window minimization / maximization, etc.
   SDL_Event event;

   // Initialize main variables
   // ------------------------------
   const unsigned int NUM_ANTS  = ants;
#ifdef EXPERIMENT
   const unsigned int NUM_WALLS = 0;
   const unsigned int NUM_FOODS = 1;
#else
   const unsigned int NUM_WALLS = walls;
   const unsigned int NUM_FOODS = foods;
#endif

   // Could make it dynamic later, but it's not that important
   const unsigned int NUM_HOMES = 1;

    // structure that holds all the objects in the enviornment
   Objs objs(NUM_ANTS, NUM_WALLS, NUM_FOODS, NUM_HOMES);


   // initialized mutex
   omp_init_lock(&objs.writelock);


   // Random wall and food creation
   generateWalls(objs.walls);
   generateFoods(objs.foods, objs.walls);

   // Could make a generator later, but it's not that important
   Rectangle home(SPAWN_LOC_X_COOR - 10, SPAWN_LOC_Y_COOR - 10, 20, 20);
   objs.homes[0] = home;

   const unsigned int TOTAL_OBJS = objs.size();
   //const unsigned int TOTAL_OBJS = ANT_DRAW_COUNT + NUM_WALLS + NUM_FOODS + NUM_HOMES;

   float delta_time;

   // Pre-allocate containers to hold every vertex and element for every ant.
   // These all get sent to GPU memory as they are processed; when finished,
   // they all get drawn in parallel by a single call to the GPU.
   // Since the vectors are pre-allocated and processed with relative indeces,
   // this method is probably already thread safe without modification!
   std::vector<float>  all_vertices(4 * 6 * TOTAL_OBJS);
   std::vector<GLuint> all_elements(    6 * TOTAL_OBJS);

   bool RUNNING = true;

#ifdef EXPERIMENT
   // statistic
   int numAntBringFoodHome = 0;
   int currentCount = 0;
   double start_time = clock();
#endif

   // main game loop
   while(RUNNING)
   {
      // Get input
      if(SDL_PollEvent(&event))
      {
         switch(event.type)
         {
            case SDL_QUIT:
               RUNNING = false;
               break;

            case SDL_KEYDOWN:
               if(event.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
               {
                  RUNNING = false;
               }

               break;
         }
      }

      // game logic
      delta_time = getDeltaTime();


#pragma omp parallel for num_threads(thread_count)
      // move the ant
      for(unsigned int i = 0; i < NUM_ANTS; ++i)
      {
         objs.ants[i].update(delta_time, objs);
      }


#ifdef EXPERIMENT
// counting the number of ants that brought food home.
// make it count as fast as possbile so it doesn't have a big
// influence on the overall time
      currentCount = 0;
#pragma omp parallel for num_threads(8) \
      reduction(+:  currentCount)
      for(unsigned int i = 0; i < NUM_ANTS; ++i){
         if(objs.ants[i].hasDeliverFood())
               ++currentCount;
      }
      if(currentCount > numAntBringFoodHome){
         numAntBringFoodHome = currentCount;
         if(numAntBringFoodHome == 64){
               double end_time = clock();
               printf("Number of ants brought food home: %d\n", numAntBringFoodHome);
               printf("Run time: %f\n", (end_time - start_time)/CLOCKS_PER_SEC);
         }
         //printf("Number of ants that brought food home: %d\n", numAntBringFoodHome);
      }
#endif


      // game rendering goes here

      // render to screen
      glClear(GL_COLOR_BUFFER_BIT);

      // OpenGL drawing is inherently parallel, so I realized we could put
      // the old style of drawing back in to demonstrate another type
      // of parallelism in our project.
      // We can compare performance of both drawing methods in our report.

      if(serial_draw)
         drawSerial(objs);
      else
         drawParallel(objs, all_vertices, all_elements);

      SDL_GL_SwapWindow(sdlwindow);
   }


    omp_destroy_lock(&objs.writelock);

   // Clean up
   SDL_GL_DeleteContext(glcontext);
   SDL_Quit();

   return 0;
}
