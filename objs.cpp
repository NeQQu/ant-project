#include "objs.hpp"

//Objs objs;

Objs::Objs(const int ANT_SIZE,
	 const int WALL_SIZE,
	 const int FOOD_SIZE,
	 const int HOME_SIZE
	)
{
	ants.resize(ANT_SIZE);
	walls.resize(WALL_SIZE);
	foods.resize(FOOD_SIZE);
	homes.resize(HOME_SIZE);
}

unsigned int Objs::size()
{
	return ants.size() + walls.size() + foods.size() + homes.size();
}
