// A container to hold all important container objects,
// so you don't need to pass a bunch of vector arguments
// to a bunch of different functions all over the code.

// openmp lock: https://stackoverflow.com/questions/2396430/how-to-use-lock-in-openmp

#ifndef OBJS_HPP
#define OBJS_HPP

#include <omp.h>
#include <vector>

#include "ant.hpp"
#include "rectangle.hpp"
#include "food.hpp"

struct Objs
{
	std::vector<Ant>       ants;
	std::vector<Rectangle> walls;
	std::vector<Food> foods;
	std::vector<Rectangle> homes;
    
    omp_lock_t writelock;

	Objs(const int ANT_SIZE,
		 const int WALL_SIZE,
		 const int FOOD_SIZE,
		 const int HOME_SIZE
		);

	unsigned int size();
};

//extern Objs objs;

#endif
