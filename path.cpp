#include "path.hpp"

#include <cmath>
#include <iostream>

#include "rectangle.hpp"

/*
Point& Point::operator = (const Point& P)
{
	if(this != &P)
	{
		x = P.x;
		y = P.y;
	}

	return *this;
}
*/

/*
Point::Point()
{
	x = 0;
	y = 0;
}

Point::Point(const int X, const int Y)
{
	x = X;
	y = Y;
}
*/

Line::Line()
{
	p1.x = 0;
	p1.y = 0;
	p2.x = 0;
	p2.y = 0;
}

Line::Line(const int X1, const int Y1, const int X2, const int Y2)
{
	p1.x = X1;
	p1.y = Y1;
	p2.x = X2;
	p2.y = Y2;
}

bool Line::collision(Line& L)
{
	//(x1 - x2)(y3 - y4) - (y1 - y2)(x3 - x4) == 0
	float den = (p1.x - p2.x) * (L.p1.y - L.p2.y) - (p1.y - p2.y) * (L.p1.x - L.p2.x);

	//std::cout << den << std::endl;

	//return (den > 0.01f) || (den < 0.01f);
	return fabs(den) < 0.01f;
}

// Slope is M in Y = M*X + B
float getSlope(const float X1, const float Y1, const float X2, const float Y2)
{
	return (Y2 - Y1) / (X2 - X1);
}

// Intercept is B in Y = M*X + B
float getIntercept(const float X, const float Y, const float M)
{
	return Y - M * X;
}

Path& Path::operator = (const Path& P)
{
	if(this != &P)
	{
		density  = P.density;
		complete = P.complete;
		expired  = P.expired;
		resolution_time_elapsed = P.resolution_time_elapsed;
		points = P.points;
	}

	return *this;
}

Path::Path()
{
	id = 0;
	density = 0;
	complete = false;
	resolution_time_elapsed = 0.0f;
}

int Path::collision(Rectangle& R)
{
	for(unsigned int i = 0; i < points.size() - 1; i++)
	{
		if(R.collision(points[i].x, points[i].y, R.w, R.h))
		{
			return i;
		}
	}

	return 0;
}

// from:
// https://stackoverflow.com/questions/1585525/how-to-find-the-intersection-point-between-a-line-and-a-rectangle
int Path::collision(const float X, const float Y, const float W, const float H)
//bool Path::collision(const float X, const float Y, const float W, const float H)
{
	if(points.size() > 1)
	{
		for(unsigned int i = 0; i < points.size() - 1; i++)
		{
			Line l0(points.at(i).x, points.at(i).y, points.at(i+1).x, points.at(i+1).y);

			Line l1(X, Y, X + W, Y); // top
			Line l2(X, Y, X, Y + H); // left
			Line l3(X + W, Y + H, X, Y + H); // bottom
			Line l4(X + W, Y + H, X + W, Y); // right

			//return (l0.collision(l1) || l0.collision(l2) || l0.collision(l3) || l0.collision(l4));

			if(l0.collision(l1) || l0.collision(l2) || l0.collision(l3) || l0.collision(l4))
			{
				return i;
			}


			//glm::vec2 p1(points.at(i).x, points.at(i).y);
			//glm::vec2 p2(points.at(i+1).x, points.at(i+1).y);
		}
	}

	/*
	if(points.size() > 1)
	{
       const float H2 = H / 2.0f;
       const float W2 = W / 2.0f;

		for(unsigned int i = 0; i < points.size() - 1; i++)
		{
			//Point& p1 = points[i];
			//Point& p2 = points[i+1];

			//Point p1(points.at(i).x, points.at(i).y);
			//Point p2(points.at(i+1).x, points.at(i+1).y);

			glm::vec2 p1(points.at(i).x, points.at(i).y);
			glm::vec2 p2(points.at(i+1).x, points.at(i+1).y);

			//float m = getSlope(p1.x, p1.y, p2.x, p2.y);

			if(((-H2) <= (m * W2) && (m * W2) <= (H2)) ||
			((-W2) <= (H2) / m && (H2) / m <= (W2))
			)
			{
				return i;
			}
		}
	}
	*/

	return 0;
}

void Path::decay()
{
	// I don't think we want incomplete paths to decay,
	// but we can discuss it later.
	if(complete)
	{
		// todo
	}
}

Path known_path;

std::vector<Path> known_paths;
