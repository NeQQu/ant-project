#ifndef PATH_HPP
#define PATH_HPP

#include <vector>

#include <glm/glm.hpp>

class Rectangle;

// Paths should not disappear while ants are still following it, but
// it can stop accepting new ants once the lifetime expires.
// Path decay will probably be exponential.

// Slope is M in Y = M*X + B
float getSlope(const float X1, const float Y1, const float X2, const float Y2);

// Intercept is B in Y = M*X + B
float getIntercept(const float X, const float Y, const float M);

/*
struct Point
{
	float x, y;

	//Point& operator = (const Point& P);

	Point();
	Point(const int X, const int Y);
};
*/

struct Line
{
	glm::vec2 p1;
	glm::vec2 p2;

	Line();
	Line(const int X1, const int Y1, const int X2, const int Y2);

	bool collision(Line& L);
};

struct Path
{
	int id;
	int density;
	bool complete;
	bool expired;
	float resolution_time_elapsed;
	//std::vector<Point> points;
	std::vector<glm::vec2> points;

	Path& operator = (const Path& P);

	Path();
	int collision(const float X, const float Y, const float W, const float H);
	//bool collision(const float X, const float Y, const float W, const float H);
	int collision(Rectangle& R);

	// I think it's better to just have the entire path decay as a single
	// object, rather than allowing each individual point to decay.
	// The latter would be a lot more work to implement, and we don't have time.
	void decay();
};

extern Path known_path;

extern std::vector<Path> known_paths;

/*
struct PathManager
{
	std::vector<Path> paths;

	//~PathManager();
	//Path& at(const int I);
};

extern PathManager known_paths;
*/

#endif
