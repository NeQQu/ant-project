#include "randomness.hpp"
#include <random>
#include <ctime>
#include <math.h>

// comment #define EXPERIMENT to run the program in normal mode
// uncomment #define EXPERIMENT to run the program in experimental mode
//#define EXPERIMENT

#ifdef EXPERIMENT
// make seed a constant value so the enviornment can be the same for each experiment run
unsigned int seed = 0;
#else
unsigned int seed = time(0);
#endif




float random_f(float min, float max){
	return min + ((float)rand_r(&seed) / ((float)RAND_MAX / (max-min) ) ) ;
}

int random_0_or_1(){
    return rand_r(&seed) % 2;
}

float random_sign()
{
	return random_0_or_1() ? 0.5f : -0.5f;
}

// returns an angle in radian
float random_angle_rad(float rad){
    static std::default_random_engine rad_generator;
    static std::normal_distribution<float> rad_distribution(0.0f, rad);
	return rad_distribution(rad_generator);
}

/*
float random_angle_rad(float degree){
    return -.52f + ((float)rand_r(&seed) / ((float)RAND_MAX / 1.04f ) ) ;
}
*/

/*
float random_angle_deg(float degree){
    static std::default_random_engine generator;
    static std::normal_distribution<float> distribution(0.0,degree);
    return distribution(generator);
}
*/
