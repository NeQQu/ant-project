#ifndef __RANDOMNESS_H__
#define __RANDOMNESS_H__

#include "constants.hpp"

extern unsigned int seed;
float random_f(float min, float max);
int random_0_or_1();

// Returns 1 or -1
float random_sign();

float random_angle_rad(float degree);
//float random_angle_deg(float degree);

#endif
