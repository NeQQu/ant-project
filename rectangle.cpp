#include "rectangle.hpp"

#include "draw.hpp"

Rectangle::Rectangle(const float X, const float Y, const float W, const float H)
{
	x = X;
	y = Y;
	w = W;
	h = H;

	r = 0;
	g = 0;
	b = 0;
}

void Rectangle::draw()
{

	drawBox(x, y, w, h, r, g, b);
}

void Rectangle::draw(std::vector<float>& all_vertices,
					std::vector<GLuint>& all_elements,
					const unsigned int i
					)
{
	drawBox(x, y, w, h, r, g, b, all_vertices, all_elements, i);
}

bool Rectangle::collision(const float X, const float Y, const float W, const float H)
{
	return ((X <= x + w) && (X + W >= x) && (Y <= y + h) && (Y + H >= y));
}


bool Rectangle::collision(const Rectangle& other){
	return ((other.x <= x + w) && (other.x + other.w >= x) && (other.y <= y + h) && (other.y + other.h >= y));
}
