#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include <vector>

// Always do this before including SDL_opengl.h
#ifndef NO_SDL_GLEXT
#define NO_SDL_GLEXT
#endif
#include <GL/glew.h>

#include <SDL_opengl.h>

class Rectangle
{
	public:
		Rectangle(const float X = 0, const float Y = 0, const float W = 20, const float H = 20);

		float x;
		float y;
		float w;
		float h;

		float r;
		float g;
		float b;

		void draw();
		void draw(std::vector<float>& all_vertices,
				  std::vector<GLuint>& all_elements,
				  const unsigned int i
				 );

		bool collision(const float X, const float Y, const float W, const float H);
		bool collision(const Rectangle& other);
};

#endif
