#version 150

in vec2 position;
in vec4 in_color;

out vec4 color;

uniform mat4 proj;
uniform float scale;
uniform vec2 offs;

void main()
{
   color = in_color;
   gl_Position = proj * vec4(position + offs, 0.0, scale);
   //gl_Position = proj * vec4(position, 0.0, 1.0);
   //gl_Position = proj * vec4(position.x + 200, position.y + 30, 0.0, 0.38);
}
